package com.snowbud56.randomtp;

/*
 * Created by snowbud56 on July 23, 2019
 * Do not change or use this code without permission
 */

import com.snowbud56.NovaUtils;
import org.bukkit.Bukkit;
import org.bukkit.World;

import java.util.Collections;
import java.util.Objects;

public class RandomTP {

    public static World portalWorld;
    public static World teleportWorld;

    public static void setUpVariables() {
        portalWorld = Bukkit.getWorld(Objects.requireNonNull(NovaUtils.getInstance().getConfig().getString("portal_world")));
        teleportWorld = Bukkit.getWorld(Objects.requireNonNull(NovaUtils.getInstance().getConfig().getString("survival_world")));
        if (NovaUtils.getInstance().getConfig().getBoolean("portal_enabled")) com.snowbud56.portals.PortalManager.initiate();
    }

    public static World getPortalWorld() {
        return portalWorld;
    }

    public static World getTeleportWorld() {
        return teleportWorld;
    }
}