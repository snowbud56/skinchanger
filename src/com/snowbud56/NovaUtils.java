package com.snowbud56;

/*
 * Created by snowbud56 on July 23, 2019
 * Do not change or use this code without permission
 */

import com.snowbud56.portals.PortalManager;
import com.snowbud56.randomtp.RandomTP;
import com.snowbud56.randomtp.command.WarpCommand;
import com.snowbud56.skinchanger.SkinCommand;
import com.snowbud56.skinchanger.SkinManager;
import com.snowbud56.staffchat.StaffChatCommand;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.Objects;

public class NovaUtils extends JavaPlugin {

    private static NovaUtils instance;

    @Override
    public void onEnable() {
        instance = this;
        RandomTP.setUpVariables();
        NovaUtils.getInstance().getConfig().options().copyDefaults(true);
        NovaUtils.getInstance().saveDefaultConfig();
        Objects.requireNonNull(NovaUtils.getInstance().getCommand("rtp")).setExecutor(new WarpCommand());
        Objects.requireNonNull(getServer().getPluginCommand("skin")).setExecutor(new SkinCommand());
        Objects.requireNonNull(getServer().getPluginCommand("sc")).setExecutor(new StaffChatCommand());
        getServer().getPluginManager().registerEvents(new SkinManager(), this);
        getServer().getPluginManager().registerEvents(new WarpCommand(), this);
        getServer().getPluginManager().registerEvents(new PortalManager(), this);
    }

    public static NovaUtils getInstance() {
        return instance;
    }
}
